# M5StickC-Plus
# https://github.com/jpcornil-git/HA-M5StickC
# https://github.com/airy10/esphome-m5stickC
substitutions:
  device_id: "1"
  devicename: "m5stickc_wiegand_${device_id}"
  friendly_devicename: "M5StickC Wiegand ${device_id}"

esphome:
  name: m5wiegand
  on_boot:
    # Splash screen for 3s then display menu 1
    priority: 800
    then:
      - delay: 3s
      - lambda: |-
          id(menu_id) = 1;
      - component.update: tft_display

esp32:
  board: m5stick-c
  framework:
    type: arduino

# Enable logging
logger:

# Enable Home Assistant API
api:
  password: ""

ota:
  password: "ota"
  platform: esphome

wifi:
  ssid: !secret wifi_ssid
  password: !secret wifi_password

  # Enable fallback hotspot (captive portal) in case wifi connection fails
  ap:
    ssid: "M5Stick Fallback Hotspot"
    password: "1hqN5oOLqJwO"

captive_portal:
    
# Built-in Serial Peripheral Interface (SPI)
spi:
  clk_pin: GPIO13
  mosi_pin: GPIO15

i2c:
  - id: i2c_internal
    sda: 21
    scl: 22
    scan: true
  - id: i2c_external
    sda: GPIO32
    scl: GPIO33

# Built-in Red Status LED Light. See https://esphome.io/components/light/status_led.html
#light:
#  - platform: status_led
#    name: ${friendly_devicename} LED Light
#    restore_mode: ALWAYS_OFF
#    pin:
#      number: GPIO10
#      inverted: true

binary_sensor:
  - platform: gpio
    pin:
      number: GPIO37
      inverted: true
    name: ${friendly_devicename} Button A
    on_press:
      then:
        - lambda: |-
            id(menu_id) += 1;
            if (id(menu_id) > 4)
              id(menu_id) = 1;
        - component.update: tft_display
  - platform: gpio
    pin:
      number: GPIO39
      inverted: true
    id: button_b
    name: ${friendly_devicename} Button B

remote_transmitter:
  pin: GPIO9
  carrier_duty_percent: 50%

sensor:
# AXP192 power management - must be present to initialize TFT power on
  - platform: axp192
    id: pmu
    address: 0x34
    i2c_id: i2c_internal
    update_interval: 30s
    battery_level:
      name: ${friendly_devicename} Battery Level
      id: "m5stick_batterylevel"

  - platform: wifi_signal
    name: ${friendly_devicename} WiFi Signal
    id: wifi_dbm
  - platform: uptime
    name: ${friendly_devicename} Uptime

wiegand:
  - id: mykeypad
    d0: GPIO0
    d1: GPIO26
    on_key:
      - lambda: |-
          ESP_LOGI("KEY", "received key %d", x);
    on_tag:
      - lambda: ESP_LOGI("TAG", "received tag %s", x.c_str());
    on_raw:
      - lambda: |-
          ESP_LOGI("RAW", "received raw %d bits, value %llx", bits, value);
          id(pacs_bits) = bits;
          id(pacs_value) = value;
      - homeassistant.tag_scanned: !lambda 'return format_hex(value);'
      - component.update: tft_display
      - text_sensor.template.publish:
          id: pacs
          state: !lambda 'return format_hex(value);'

text_sensor:
  - platform: template
    name: "${friendly_devicename} pacs"
    id: pacs

globals:
  - id: menu_id
    type: int
    restore_value: no
    initial_value: "0"
  - id: pacs_bits
    type: int
    restore_value: no
    initial_value: '0'
  - id: pacs_value
    type: uint64_t
    restore_value: no
    initial_value: '0'

font:
  - file: 'arial.ttf'
    id: font1
    size: 14
  - file: 'arial.ttf'
    id: font2
    size: 24
  - file: 'arial.ttf'
    id: font3
    size: 36

# Colors
color:
  - id: red
    red: 100%
    green: 0%
    blue: 0%
  - id: green
    red: 0%
    green: 100%
    blue: 0%
  - id: blue
    red: 0%
    green: 0%
    blue: 100%
  - id: gray
    red: 50%
    green: 50%
    blue: 50%


# 135x240 TFT LCD
display:
  - platform: st7789v
    id: tft_display
    model: TTGO TDisplay 135x240
    cs_pin: GPIO5
    dc_pin: GPIO23
    reset_pin: GPIO18
    rotation: 90
    lambda: |-
      float brightness=0.5;
      if (id(pacs_bits) > 0) {
        it.printf(0, 40, id(font3), green, TextAlign::TOP_LEFT, "(%d)", id(pacs_bits));
        it.printf(0, 90, id(font2), green, TextAlign::TOP_LEFT, "%llx", id(pacs_value));
      } else {
        it.print(0, 20, id(font1), red, TextAlign::LEFT, "Ready...");
      }
      // Update display brightness
      id(pmu).set_brightness(brightness);
      id(pmu).update();

time:
  - platform: homeassistant
    id: homeassistant_time
  - platform: sntp
    id: sntp_time
